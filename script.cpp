/*
	THIS FILE IS A PART OF RDR 2 SCRIPT HOOK SDK
				http://dev-c.com
			(C) Alexander Blade 2019
*/

#include "script.h"
#include "ini.h"
#include <string>
#include <vector>
#include <filesystem>

bool enabled;
float pedScaleMale, pedScaleFemale, pedScaleAnimals, playerScale;

void readconfig()
{
	try
	{
		INI ini(".\\MiniNPCs.ini");
		enabled = ini.getBool("SETTINGS", "Enabled", true);
		pedScaleMale = ini.getFloat("SETTINGS", "PedScaleMale", 0.5f);
		pedScaleFemale = ini.getFloat("SETTINGS", "PedScaleFemale", 0.5f);
		pedScaleAnimals = ini.getFloat("SETTINGS", "PedScaleAnimals", 0.5f);
		playerScale = ini.getFloat("SETTINGS", "PlayerScale", 1.0f);
	}
	catch (...)
	{
		enabled = true;
		pedScaleMale = 0.5f;
		pedScaleFemale = 0.5f;
		pedScaleAnimals = 0.5f;
		playerScale = 1.0f;
	}
}

Hash joaat(const char* str)
{
	return MISC::GET_HASH_KEY(str);
}

void update()
{
	if (enabled != false)
	{
		// player
		Player player = PLAYER::PLAYER_ID();
		Ped playerPed = PLAYER::PLAYER_PED_ID();

		const int ARR_SIZE = 1024;
		Ped peds[ARR_SIZE];
		int count = worldGetAllPeds(peds, ARR_SIZE);

		for (int i = 0; i < count; i++)
			if (peds[i] != playerPed)
			{
				if (PED::IS_PED_HUMAN(peds[i]))
				{
						if (PED::IS_PED_MALE(peds[i]))
						{
							PED::_SET_PED_SCALE(peds[i], pedScaleMale);
						}
						else
						{
							PED::_SET_PED_SCALE(peds[i], pedScaleFemale);
						}
				}
				else
				{
					PED::_SET_PED_SCALE(peds[i], pedScaleAnimals);
				}
			}

		PED::_SET_PED_SCALE(playerPed, playerScale);
	}
}

void main()
{
	while (true)
	{
		update();
		readconfig();
		WAIT(2000);
	}
}

void ScriptMain()
{
	srand(GetTickCount());
	main();
}