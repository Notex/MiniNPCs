#pragma once

#include <Windows.h>
#include <string>

class INI
{
	LPCSTR filename;

public:
	INI(LPCSTR file) { filename = file; }
	std::string getString(LPCSTR section, LPCSTR key, LPCSTR default);
	BOOL getBool(LPCSTR section, LPCSTR key, BOOL default);
	int getInt(LPCSTR section, LPCSTR key, int default);
	float getFloat(LPCSTR section, LPCSTR key, float default);
	void setString(LPCSTR section, LPCSTR key, LPCSTR value);
	void setBool(LPCSTR section, LPCSTR key, BOOL value);
	void setInt(LPCSTR section, LPCSTR key, int value);
	void setFloat(LPCSTR section, LPCSTR key, float value);
};