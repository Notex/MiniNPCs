#include "ini.h"

std::string INI::getString(LPCSTR section, LPCSTR key, LPCSTR default)
{
	char temp[1024];
	int result = GetPrivateProfileString(section, key, default, temp, sizeof(temp), INI::filename);
	return std::string(temp, result);
}

BOOL INI::getBool(LPCSTR section, LPCSTR key, BOOL default)
{
	char szResult[255];
	char szDefault[255];
	bool bolResult;
	sprintf(szDefault, "%s", default ? "TRUE" : "FALSE");
	GetPrivateProfileString(section, key, szDefault, szResult, 255, INI::filename);
	bolResult = (strcmp(szResult, "TRUE") == 0 ||
		strcmp(szResult, "true") == 0) ? true : false;
	return bolResult;
}

int INI::getInt(LPCSTR section, LPCSTR key, int default)
{
	return GetPrivateProfileInt(section, key, default, INI::filename);
}

float INI::getFloat(LPCSTR section, LPCSTR key, float default)
{
	char temp[1024];
	int result = GetPrivateProfileString(section, key, "", temp, sizeof(temp), INI::filename);

	return std::stof(std::string(temp, result));
}

// Need administrative rights for setting ini values below

void INI::setBool(LPCSTR section, LPCSTR key, BOOL default)
{
	WritePrivateProfileString(section, key, default ? "TRUE" : "FALSE", INI::filename);
}

void INI::setString(LPCSTR section, LPCSTR key, LPCSTR value)
{
	WritePrivateProfileString(section, key, value, INI::filename);
}

void INI::setInt(LPCSTR section, LPCSTR key, int value)
{
	WritePrivateProfileString(section, key, std::to_string(value).c_str(), INI::filename);
}

void INI::setFloat(LPCSTR section, LPCSTR key, float value)
{
	WritePrivateProfileString(section, key, std::to_string(value).c_str(), INI::filename);
}